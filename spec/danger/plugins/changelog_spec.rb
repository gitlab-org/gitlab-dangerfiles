# frozen_string_literal: true

require "spec_helper"

RSpec.describe Danger::Changelog do
  include_context "with dangerfile"

  let(:changelog_path) { "changelog-path.yml" }
  let(:foss_change) { nil }
  let(:ee_change) { nil }
  let(:changelog_change) { nil }
  let(:changes) { changes_class.new([foss_change, ee_change, changelog_change].compact) }

  it "is a plugin" do
    expect(described_class < Danger::Plugin).to be_truthy
  end

  subject(:changelog) { dangerfile.changelog }

  before do
    allow(changelog.helper).to receive(:changed_files).with(%r{\Aee/}).and_return([ee_change&.file].compact)
    allow(changelog.helper).to receive(:config).and_return(double(project_root: ""))
    allow(File).to receive(:exist?).and_return(true)
  end

  describe "#check!" do
    context "when the MR is a revert MR" do
      before do
        allow(changelog.helper).to receive(:revert_mr?).and_return(true)
      end

      context "when the revert is not for the current milestone" do
        before do
          allow(changelog).to receive(:revert_in_current_milestone?).and_return(false)
        end

        it "does the changelog checks" do
          expect(changelog).to receive(:critical_checks)
          expect(changelog).to receive(:regular_checks)
          expect(changelog).to receive(:changelog_categories_checks)

          changelog.check!
        end

        it "displays a warning" do
          # We don't mock critical_checks, because we are testing its output
          allow(changelog).to receive(:regular_checks)
          allow(changelog).to receive(:changelog_categories_checks)

          allow(changelog.helper).to receive(:stable_branch?).and_return(false)

          expect_changelog_result = described_class::ChangelogCheckResult.empty

          expected_warning = <<~MARKDOWN
            In a revert merge request? Use the revert merge request template to add labels [that skip changelog checks](https://docs.gitlab.com/ee/development/pipelines#revert-mrs).

            Reverting something in the current milestone? A changelog isn't required. Skip changelog checks by adding `~"regression:*"` label, then re-run the danger job (there is a link at the bottom of this comment).
          MARKDOWN

          expect_changelog_result.warning(expected_warning)

          expect(changelog).to receive(:add_danger_messages).with(have_attributes(warnings: expect_changelog_result.warnings))

          changelog.check!
        end
      end

      context "when the merge request targets a stable branch" do
        it "skips the warning" do
          allow(changelog.helper).to receive_messages(current_milestone: double(title: "16.0"), mr_labels: %w[foo], revert_mr?: true, stable_branch?: true)

          allow(changelog).to receive(:regular_checks)
          allow(changelog).to receive(:changelog_categories_checks)

          expect(changelog).to receive(:add_danger_messages).with(have_attributes(warnings: []))

          changelog.check!
        end
      end

      context "when the revert is for the current milestone" do
        before do
          allow(changelog).to receive(:revert_in_current_milestone?).and_return(true)
        end

        it "does not do changelog checks" do
          expect(changelog).not_to receive(:critical_checks)
          expect(changelog).not_to receive(:regular_checks)
          expect(changelog).not_to receive(:changelog_categories_checks)

          changelog.check!
        end

        it "does not display any warnings" do
          expect(changelog).not_to receive(:add_danger_messages)

          changelog.check!
        end
      end
    end

    context "when the MR is not a revert MR" do
      before do
        allow(changelog.helper).to receive(:revert_mr?).and_return(false)
      end

      it "does not display a warning" do
        # We don't mock critical_checks, because we are testing its output
        allow(changelog).to receive(:regular_checks)
        allow(changelog).to receive(:changelog_categories_checks)

        expect(changelog).to receive(:add_danger_messages).with(have_attributes(warnings: []))

        changelog.check!
      end

      it "performs the changelog checks" do
        expect(changelog).to receive(:critical_checks)
        expect(changelog).to receive(:regular_checks)
        expect(changelog).to receive(:changelog_categories_checks)

        changelog.check!
      end
    end
  end

  describe "#categories" do
    it "detects the categories set on all the valid commits" do
      commits = [
        double(:commit, message: "foo\nChangelog: foo"),
        double(:commit, message: "foo\nChangelog: fixed"),
        double(:commit, message: "foo\nChangelog: added"),
        double(:commit, message: "foo\nChangelog: security"),
      ]

      allow(changelog.git).to receive(:commits).and_return(commits)

      expect(changelog.categories).to match_array(%w[fixed added security])
    end
  end

  describe "#check_changelog_commit_categories" do
    context "when all changelog commits are correct" do
      it "does not produce any messages" do
        commit = double(:commit, message: "foo\nChangelog: fixed")

        allow(changelog.git).to receive(:commits).and_return([commit])

        expect(changelog).not_to receive(:fail)

        changelog.check_changelog_commit_categories
      end
    end

    context "when a commit has an incorrect trailer" do
      it "adds a message" do
        commit = double(:commit, message: "foo\nChangelog: foo", sha: "123")

        allow(changelog.git).to receive(:commits).and_return([commit])

        expect(changelog).to receive(:fail)

        changelog.check_changelog_commit_categories
      end
    end
  end

  describe "#revert_in_current_milestone?" do
    context "when the MR is not a revert MR" do
      before do
        allow(changelog.helper).to receive(:revert_mr?).and_return(false)
      end

      it "returns false" do
        expect(changelog.revert_in_current_milestone?).to be_falsey
      end
    end

    context "when the MR is a revert MR" do
      let(:current_milestone_title) { "16.0" }

      before do
        allow(changelog.helper).to receive_messages(revert_mr?: true, current_milestone: double(title: current_milestone_title))
      end

      context "when the MR does not contain any regression label" do
        before do
          allow(changelog.helper).to receive(:mr_labels).and_return(%w[a-label])
        end

        it "returns false" do
          expect(changelog.revert_in_current_milestone?).to be_falsey
        end
      end

      context "when the MR contains a regression label" do
        before do
          allow(changelog.helper).to receive(:mr_labels).and_return([regression_label])
        end

        context "when the regression label is not from the current milestone" do
          let(:regression_label) { "regression:15.0" }

          it "returns false" do
            expect(changelog.revert_in_current_milestone?).to be_falsey
          end
        end

        context "when the regression label is from the current milestone" do
          let(:regression_label) { "regression:#{current_milestone_title}" }

          it "returns true" do
            expect(changelog.revert_in_current_milestone?).to be_truthy
          end
        end
      end

      context "when in local context" do
        before do
          allow(dangerfile.helper).to receive(:ci?).and_return(false)
        end

        it "returns true" do
          expect(changelog.revert_in_current_milestone?).to be_truthy
        end
      end
    end
  end

  describe "#check_changelog_trailer" do
    subject { changelog.check_changelog_trailer(commit) }

    context "when categories config file doesn't exist but category is part of the default ones" do
      let(:commit) { described_class::CommitWrapper.new(double("commit", sha: "abc123"), "Changelog", "fixed") }

      before do
        allow(File).to receive(:exist?).and_return(false)
      end

      it { is_expected.to have_attributes(errors: []) }
    end

    context "when categories config file has invalid YAML but category is part of the default ones" do
      let(:commit) { described_class::CommitWrapper.new(double("commit", sha: "abc123"), "Changelog", "fixed") }

      before do
        allow(YAML).to receive(:load_file).and_raise(Psych::SyntaxError)
      end

      it { is_expected.to have_attributes(errors: []) }
    end

    context "when commit include a changelog trailer with an unknown category" do
      let(:commit) { described_class::CommitWrapper.new(double("commit", sha: "abc123"), "Changelog", "foo") }

      it { is_expected.to have_attributes(errors: ["Commit #{commit.sha} uses an invalid changelog category: foo"]) }
    end

    context "when a commit uses the wrong casing for a trailer" do
      let(:commit) { described_class::CommitWrapper.new(double("commit", sha: "abc123"), "changelog", "foo") }

      it { is_expected.to have_attributes(errors: ["The changelog trailer for commit #{commit.sha} must be `Changelog` (starting with a capital C), not `changelog`"]) }
    end

    context "when commit include a changelog trailer with category set to 'fixed'" do
      let(:commit) { described_class::CommitWrapper.new(double("commit", sha: "abc123"), "Changelog", "fixed") }

      it { is_expected.to have_attributes(errors: []) }
    end
  end

  describe "#check_changelog_path" do
    before do
      allow(changelog).to receive(:exist?).and_return(true)
    end

    subject { changelog.check_changelog_path }

    context "when changelog is not present" do
      before do
        allow(changelog).to receive(:exist?).and_return(false)
      end

      it { is_expected.to have_attributes(errors: [], warnings: [], markdowns: [], messages: []) }
    end

    context "with EE changes" do
      let(:ee_change) { change_class.new("ee/app/models/foo.rb", :added, :backend) }

      context "and a non-EE changelog, and changelog not required" do
        before do
          allow(changelog).to receive_messages(required?: false, ee_changelog?: false)
        end

        it { is_expected.to have_attributes(warnings: ["This MR changes code in `ee/`, but its Changelog commit is missing the [`EE: true` trailer](https://docs.gitlab.com/ee/development/changelog.html#gitlab-enterprise-changes). Consider adding it to your Changelog commits."]) }
      end

      context "and a EE changelog" do
        before do
          allow(changelog).to receive(:ee_changelog?).and_return(true)
        end

        it { is_expected.to have_attributes(errors: [], warnings: [], markdowns: [], messages: []) }

        context "and there are DB changes" do
          let(:foss_change) { change_class.new("db/migrate/foo.rb", :added, :migration) }

          it { is_expected.to have_attributes(warnings: ["This MR has a Changelog commit with the `EE: true` trailer, but there are database changes which [requires](https://docs.gitlab.com/ee/development/changelog.html#what-warrants-a-changelog-entry) the Changelog commit to not have the `EE: true` trailer. Consider removing the `EE: true` trailer from your commits."]) }
        end
      end
    end

    context "with no EE changes" do
      let(:foss_change) { change_class.new("app/models/foo.rb", :added, :backend) }

      context "and a non-EE changelog" do
        before do
          allow(changelog).to receive(:ee_changelog?).and_return(false)
        end

        it { is_expected.to have_attributes(errors: [], warnings: [], markdowns: [], messages: []) }
      end

      context "and a EE changelog" do
        before do
          allow(changelog).to receive(:ee_changelog?).and_return(true)
        end

        it { is_expected.to have_attributes(warnings: ["This MR has a Changelog commit for EE, but no code changes in `ee/`. Consider removing the `EE: true` trailer from your commits."]) }
      end
    end
  end

  describe "#required_reasons" do
    subject { changelog.required_reasons }

    context "added files contain a migration" do
      let(:changes) { changes_class.new([change_class.new("foo", :added, :migration)]) }

      it { is_expected.to include(:db_changes) }
    end

    context "removed files contains a feature flag" do
      let(:changes) { changes_class.new([change_class.new("foo", :deleted, :feature_flag)]) }

      it { is_expected.to include(:feature_flag_removed) }
    end

    context "added files do not contain a migration" do
      let(:changes) { changes_class.new([change_class.new("foo", :added, :frontend)]) }

      it { is_expected.to be_empty }
    end

    context "removed files do not contain a feature flag" do
      let(:changes) { changes_class.new([change_class.new("foo", :deleted, :backend)]) }

      it { is_expected.to be_empty }
    end
  end

  describe "#required?" do
    subject { changelog.required? }

    context "added files contain a migration" do
      let(:changes) { changes_class.new([change_class.new("foo", :added, :migration)]) }

      it { is_expected.to be_truthy }
    end

    context "removed files contains a feature flag" do
      let(:changes) { changes_class.new([change_class.new("foo", :deleted, :feature_flag)]) }

      it { is_expected.to be_truthy }
    end

    context "added files do not contain a migration" do
      let(:changes) { changes_class.new([change_class.new("foo", :added, :frontend)]) }

      it { is_expected.to be_falsey }
    end

    context "removed files do not contain a feature flag" do
      let(:changes) { changes_class.new([change_class.new("foo", :deleted, :backend)]) }

      it { is_expected.to be_falsey }
    end
  end

  describe "#optional?" do
    let(:category_with_changelog) { :backend }
    let(:label_with_changelog) { "frontend" }
    let(:category_without_changelog) { described_class::NO_CHANGELOG_CATEGORIES.first }
    let(:label_without_changelog) { described_class::NO_CHANGELOG_LABELS.first }
    let(:mr_labels) { [] }

    subject { changelog.optional? }

    before do
      allow(changelog.helper).to receive(:mr_labels).and_return(mr_labels)
    end

    context "when MR contains only categories requiring no changelog" do
      let(:changes) { changes_class.new([change_class.new("foo", :modified, category_without_changelog)]) }

      it "is falsey" do
        is_expected.to be_falsy
      end
    end

    context "when MR contains a label that require no changelog" do
      let(:changes) { changes_class.new([change_class.new("foo", :modified, category_with_changelog)]) }
      let(:mr_labels) { [label_with_changelog, label_without_changelog] }

      it "is falsey" do
        is_expected.to be_falsy
      end
    end

    context "when MR contains a category that require changelog and a category that require no changelog" do
      let(:changes) { changes_class.new([change_class.new("foo", :modified, category_with_changelog), change_class.new("foo", :modified, category_without_changelog)]) }

      context "with no labels" do
        it "is truthy" do
          is_expected.to be_truthy
        end
      end

      context "with changelog label" do
        let(:mr_labels) { ["type::feature"] }

        it "is truthy" do
          is_expected.to be_truthy
        end
      end

      context "with no changelog label" do
        let(:mr_labels) { ["maintenance::workflow"] }

        it "is truthy" do
          is_expected.to be_falsey
        end
      end
    end
  end

  describe "#exist?" do
    it "returns true when a Changelog commit exists" do
      allow(changelog).to receive(:valid_changelog_commits)
                            .and_return([double(:commit)])

      expect(changelog.exist?).to be_truthy
    end

    it "returns false when a Changelog commit is missing" do
      allow(changelog).to receive(:valid_changelog_commits).and_return([])

      expect(changelog.exist?).to be_falsy
    end
  end

  describe "#changelog_commits" do
    it "returns the commits that include a Changelog trailer" do
      commit1 = double(:commit, message: "foo\nChangelog: fixed", sha: "1")
      commit2 = double(:commit, message: "bar\nChangelog: kittens", sha: "2")
      commit3 = double(:commit, message: "testing", sha: "3")

      allow(changelog.git).to receive(:commits).and_return([commit1, commit2, commit3])

      expect(changelog.changelog_commits.map(&:sha)).to contain_exactly(commit1.sha, commit2.sha)
    end
  end

  describe "#valid_changelog_commits" do
    it "returns the commits with a valid Changelog trailer" do
      commit1 = double(:commit, message: "foo\nChangelog: fixed", sha: "1")
      commit2 = double(:commit, message: "bar\nChangelog: kittens", sha: "2")

      allow(changelog.git).to receive(:commits)
                                .and_return([commit1, commit2])

      expect(changelog.valid_changelog_commits.map(&:sha)).to contain_exactly(commit1.sha)
    end
  end

  describe "#ee_changelog?" do
    it "returns true when an EE changelog commit is present" do
      commit = double(:commit, message: "foo\nChangelog: fixed\nEE: true")

      allow(changelog.git).to receive(:commits).and_return([commit])

      expect(changelog.ee_changelog?).to eq(true)
    end

    it "returns false when an EE changelog commit is missing" do
      commit = double(:commit, message: "foo\nChangelog: fixed")

      allow(changelog.git).to receive(:commits).and_return([commit])

      expect(changelog.ee_changelog?).to eq(false)
    end
  end

  describe "#modified_text" do
    subject { changelog.modified_text }

    context "when in CI context" do
      shared_examples "changelog modified text" do |_key|
        specify do
          expect(subject).to include("CHANGELOG.md was edited")
          expect(subject).to include("`Changelog` trailer")
        end
      end

      before do
        allow(dangerfile.helper).to receive(:ci?).and_return(true)
      end

      context "when title is not changed from sanitization", :aggregate_failures do
        let(:mr_title) { "Fake Title" }

        it_behaves_like "changelog modified text"
      end

      context "when title needs sanitization", :aggregate_failures do
        let(:mr_title) { "DRAFT: Fake Title" }

        it_behaves_like "changelog modified text"
      end
    end

    context "when in local context" do
      let(:mr_title) { "Fake Title" }

      before do
        allow(dangerfile.helper).to receive(:ci?).and_return(false)
      end

      specify do
        expect(subject).to include("CHANGELOG.md was edited")
        expect(subject).not_to include("`Changelog` trailer")
      end
    end
  end

  describe "#required_texts" do
    let(:mr_title) { "Fake Title" }

    subject { changelog.required_texts }

    context "when in CI context" do
      before do
        allow(dangerfile.helper).to receive(:ci?).and_return(true)
      end

      shared_examples "changelog required text" do |key|
        specify do
          expect(subject).to have_key(key)
          expect(subject[key]).to include("CHANGELOG missing")
          expect(subject[key]).to include("`Changelog` trailer")
        end
      end

      context "with a new migration file" do
        let(:changes) { changes_class.new([change_class.new("foo", :added, :migration)]) }

        context "when title is not changed from sanitization", :aggregate_failures do
          it_behaves_like "changelog required text", :db_changes
        end

        context "when title needs sanitization", :aggregate_failures do
          let(:mr_title) { "DRAFT: Fake Title" }

          it_behaves_like "changelog required text", :db_changes
        end
      end

      context "with a removed feature flag file" do
        let(:changes) { changes_class.new([change_class.new("foo", :deleted, :feature_flag)]) }

        it_behaves_like "changelog required text", :feature_flag_removed
      end
    end

    context "when in local context" do
      before do
        allow(dangerfile.helper).to receive(:ci?).and_return(false)
      end

      shared_examples "changelog required text" do |key|
        specify do
          expect(subject).to have_key(key)
          expect(subject[key]).to include("CHANGELOG missing")
          expect(subject[key]).not_to include("`Changelog` trailer")
        end
      end

      context "with a new migration file" do
        let(:changes) { changes_class.new([change_class.new("foo", :added, :migration)]) }

        context "when title is not changed from sanitization", :aggregate_failures do
          it_behaves_like "changelog required text", :db_changes
        end

        context "when title needs sanitization", :aggregate_failures do
          let(:mr_title) { "DRAFT: Fake Title" }

          it_behaves_like "changelog required text", :db_changes
        end
      end

      context "with a removed feature flag file" do
        let(:changes) { changes_class.new([change_class.new("foo", :deleted, :feature_flag)]) }

        it_behaves_like "changelog required text", :feature_flag_removed
      end
    end
  end

  describe "#optional_text" do
    subject { changelog.optional_text }

    context "when in CI context" do
      shared_examples "changelog optional text" do |_key|
        specify do
          expect(subject).to include("CHANGELOG missing")
          expect(subject).to include("`Changelog` trailer")
        end
      end

      before do
        allow(dangerfile.helper).to receive(:ci?).and_return(true)
      end

      context "when title is not changed from sanitization", :aggregate_failures do
        let(:mr_title) { "Fake Title" }

        it_behaves_like "changelog optional text"
      end

      context "when title needs sanitization", :aggregate_failures do
        let(:mr_title) { "DRAFT: Fake Title" }

        it_behaves_like "changelog optional text"
      end
    end

    context "when in local context" do
      let(:mr_title) { "Fake Title" }

      before do
        allow(dangerfile.helper).to receive(:ci?).and_return(false)
      end

      specify do
        expect(subject).to include("CHANGELOG missing")
      end
    end
  end
end

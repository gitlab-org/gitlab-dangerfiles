# frozen_string_literal: true

require "spec_helper"

RSpec.describe Danger::Roulette do
  include_context "with dangerfile"
  include_context "with teammates"

  around do |example|
    Timecop.travel(Time.utc(2020, 06, 22, 10)) { example.run }
  end

  it "is a plugin" do
    expect(described_class < Danger::Plugin).to be_truthy
  end

  let(:project) { "gitlab" }
  let(:author) { Gitlab::Dangerfiles::Teammate.new("username" => "johndoe") }
  let(:mr_source_branch) { "a-branch" }

  subject(:roulette) { dangerfile.roulette }

  before do
    allow(dangerfile.helper).to receive(:mr_source_branch).and_return(mr_source_branch)
  end

  describe "#prepare_categories" do
    let(:mr_labels) { [] }

    before do
      allow(dangerfile.helper).to receive(:mr_labels).and_return(mr_labels)
    end

    context "when there is UX label and Community contribution" do
      let(:mr_labels) { ["UX", "Community contribution"] }

      it "adds :ux to categories" do
        expect(roulette.prepare_categories([])).to include(:ux)
      end
    end

    context "when there is UX label and a group label" do
      let(:mr_labels) { ["UX", group_label] }

      before do
        allow(dangerfile.helper).to receive(:mr_author).and_return(author.username)
        allow(roulette).to receive_messages(config_project_name: project, teammate_pedroms: teammate_pedroms)

        WebMock
          .stub_request(:get, Gitlab::Dangerfiles::Teammate::ROULETTE_DATA_URL)
          .to_return(body: teammate_json)
      end

      context "when there is a matching group label" do
        let(:group_label) { "group::source code" }

        it "adds :ux to categories" do
          expect(roulette.prepare_categories([])).to include(:ux)
        end
      end

      context "when there is no matching group label" do
        let(:group_label) { "group::non-existing group" }

        it "does not add :ux to categories" do
          expect(roulette.prepare_categories([])).not_to include(:ux)
        end
      end
    end
  end

  describe "#team_mr_author" do
    context "when author is a teammate" do
      before do
        allow(dangerfile.helper).to receive(:mr_author).and_return(backend_maintainer.username)
      end

      it "calls Teammate.find_member with the merge request author" do
        expect(roulette.team_mr_author).to eq(backend_maintainer)
      end
    end

    context "when author is not a teammate" do
      before do
        allow(dangerfile.helper).to receive(:mr_author).and_return('wild_contributor')
      end

      it "calls Teammate.find_member with the merge request author" do
        expect(roulette.team_mr_author).to be_nil
      end
    end
  end

  describe "#spin" do
    let(:mr_labels) { ["backend", "devops::create"] }
    let(:spins) do
      # Stub the request at the latest time so that we can modify the raw data, e.g. available fields.
      WebMock
        .stub_request(:get, Gitlab::Dangerfiles::Teammate::ROULETTE_DATA_URL)
        .to_return(body: teammate_json)

      subject.spin(project, categories)
    end

    before do
      allow(dangerfile.helper).to receive_messages(mr_author: author.username, mr_labels: mr_labels)
    end

    context "when change contains backend category" do
      let(:categories) { [:backend] }

      it "assigns backend reviewer and maintainer" do
        expect(spins[0].reviewer).to eq(backend_reviewer)
        expect(spins[0].maintainer).to eq(backend_maintainer)
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:backend, backend_reviewer, backend_maintainer, false)])
      end

      context "when teammate is not available" do
        let(:backend_available) { false }

        it "assigns backend reviewer and no maintainer" do
          expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:backend, backend_reviewer, nil, false)])
        end
      end

      context "when no reviewers are available and there are multiple maintainers available" do
        let(:teammates) do
          [
            backend_maintainer.to_h,
            another_backend_maintainer.to_h,
          ]
        end

        it "assigns backend reviewer from maintainer and anther maintainer as maintainer" do
          expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:backend, another_backend_maintainer, backend_maintainer, false)])
        end
      end

      context "when no reviewers are available and there is one maintainer available" do
        let(:teammates) do
          [
            backend_maintainer.to_h,
          ]
        end

        it "assigns backend maintainer and no reviewer" do
          expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:backend, nil, backend_maintainer, false)])
        end
      end
    end

    context "when change contains frontend category" do
      let(:categories) { [:frontend] }

      it "assigns frontend reviewer and maintainer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:frontend, frontend_reviewer, frontend_maintainer, false)])
      end
    end

    context "when change contains many categories" do
      let(:categories) { [:frontend, :test, :qa, :tooling, :ci_template, :backend, :ux, :none] }

      it "has a deterministic sorting order" do
        expect(spins.map(&:category)).to eq categories.sort_by(&:to_s)
      end
    end

    context "when change contains QA category" do
      let(:categories) { [:qa] }

      it "assigns QA maintainer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:qa, nil, software_engineer_in_test, false)])
      end
    end

    context "when change contains UX category" do
      let(:categories) { [:ux] }
      let(:mr_labels) { ["group::source code"] }

      it "assigns UX reviewer and maintainer is optional" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:ux, ux_reviewer, nil, :maintainer)])
      end

      context "when it is a wider community contribution without a group" do
        let(:mr_labels) { ["Community contribution"] }

        before do
          allow(roulette).to receive(:teammate_pedroms).and_return(teammate_pedroms)
        end

        it "assigns Pedro" do
          expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:ux, teammate_pedroms, nil, :maintainer)])
        end
      end

      context "when it is a wider community contribution with source code group" do
        let(:mr_labels) { ["Community contribution", "group::source code"] }

        it "assigns source code UX reviewer" do
          expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:ux, ux_reviewer, nil, :maintainer)])
        end
      end
    end

    context "when change contains Analytics Instrumentation category" do
      let(:categories) { [:analytics_instrumentation] }

      it "assigns Analytics Instrumentation reviewer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:analytics_instrumentation, analytics_instrumentation_reviewer, backend_maintainer, :maintainer)])
      end
    end

    shared_examples "assigns Import and Integrate Backend review correctly" do
      it "assigns Import and Integrate Backend review, and no maintainer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:import_integrate_be, import_and_integrate_backend_reviewer, nil, :maintainer)])
      end
    end

    shared_examples "assigns Import and Integrate Frontend review correctly" do
      it "assigns Import and Integrate Frontend review, and no maintainer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:import_integrate_fe, import_and_integrate_frontend_reviewer, nil, :maintainer)])
      end
    end

    context "when change contains Import and Integrate Backend category" do
      let(:categories) { [:import_integrate_be] }

      it_behaves_like "assigns Import and Integrate Backend review correctly"

      context "when author is an Import and Integrate Backend team member" do
        let!(:author) { software_engineer_in_import_integrate_be }

        it "does not assign a reviewer" do
          expect(spins).to be_empty
        end
      end

      context "when author is an Import and Integrate Frontend team member" do
        let!(:author) { software_engineer_in_import_integrate_fe }

        it_behaves_like "assigns Import and Integrate Backend review correctly"
      end
    end

    context "when change contains Import and Integrate Frontend category" do
      let(:categories) { [:import_integrate_fe] }

      it_behaves_like "assigns Import and Integrate Frontend review correctly"

      context "when author is an Import and Integrate Frontend team member" do
        let!(:author) { software_engineer_in_import_integrate_fe }

        it "does not assign a reviewer" do
          expect(spins).to be_empty
        end
      end

      context "when author is an Import and Integrate Backend team member" do
        let!(:author) { software_engineer_in_import_integrate_be }

        it_behaves_like "assigns Import and Integrate Frontend review correctly"
      end
    end

    context "when change contains QA category and another category" do
      let(:categories) { [:backend, :qa] }

      it "assigns QA maintainer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:backend, backend_reviewer, backend_maintainer, false), Gitlab::Dangerfiles::Spin.new(:qa, nil, software_engineer_in_test, :maintainer)])
      end

      context "and author is an SET" do
        let!(:author) { Gitlab::Dangerfiles::Teammate.new("username" => software_engineer_in_test.username) }

        it "assigns QA reviewer" do
          expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:backend, backend_reviewer, backend_maintainer, false), Gitlab::Dangerfiles::Spin.new(:qa, nil, nil, false)])
        end
      end
    end

    context "when change contains Engineering Productivity category" do
      let(:categories) { [:tooling] }

      context "when no backend reviewer is available" do
        let(:backend_reviewer_available) { false }

        it "assigns Engineering Productivity reviewer and fallback to backend maintainer" do
          expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:tooling, tooling_reviewer, backend_maintainer, false)])
        end
      end
    end

    context "when change contains CI/CD Template category" do
      let(:categories) { [:ci_template] }

      it "assigns CI/CD Template reviewer and fallback to backend maintainer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:ci_template, ci_template_reviewer, backend_maintainer, false)])
      end
    end

    context "when change contains test category" do
      let(:categories) { [:test] }

      it "assigns corresponding SET" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:test, software_engineer_in_test, nil, :maintainer)])
      end
    end

    context "when project has no categories" do
      let(:project) { "gitlab-workhorse" }
      let(:categories) { [:none] }

      it "assigns project reviewer & maintainer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:none, workhorse_reviewer, workhorse_maintainer, false)])
      end
    end

    context "when project has legacy nil category" do
      let(:project) { "gitlab-workhorse" }
      let(:categories) { [nil] }

      it "assigns project reviewer & maintainer" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:none, workhorse_reviewer, workhorse_maintainer, false)])
      end
    end

    context "when project has a category that's uppercase" do
      let(:categories) { [:QA] }

      it "is made lowercase" do
        expect(spins).to eq([Gitlab::Dangerfiles::Spin.new(:qa, nil, software_engineer_in_test, false)])
      end
    end
  end

  describe "#codeowners_approvals" do
    context "when gitlab_helper is not available" do
      before do
        allow(dangerfile.helper).to receive(:gitlab_helper).and_return(nil)
      end

      it "returns an empty array" do
        expect(subject.codeowners_approvals).to be_empty
      end
    end

    context "when gitlab_helper is available" do
      before do
        allow(dangerfile.helper.config).to receive(:project_name).and_return("gitlab")

        WebMock
          .stub_request(:get, Gitlab::Dangerfiles::Teammate::ROULETTE_DATA_URL)
          .to_return(body: teammate_json)

        allow(dangerfile.helper).to receive_messages(mr_author: { "username" => "johndoe" }, mr_iid: "123", mr_target_project_id: "1234", mr_approval_state: return_json)
      end

      context "when there is a single approval rule" do
        let(:rule) do
          {
            "id" => 333,
            "name" => "*.rb",
            "rule_type" => "code_owner",
            "eligible_approvers" => [valid_reviewer, invalid_reviewer],
            "approvals_required" => number_of_approvals_required,
            "section" => "Backend",
            "code_owner" => true
          }
        end

        let(:valid_reviewer) do
          {
            "id" => 123,
            "username" => backend_maintainer.username,
            "name" => backend_maintainer.name,
            "state" => "active"
          }
        end

        let(:invalid_reviewer) do
          {
            "id" => 321,
            "username" => "not-reviewer",
            "name" => "Not Reviewer",
            "state" => "active"
          }
        end

        let(:return_json) { { "rules" => [rule] } }

        context "no required approvals are needed" do
          let(:number_of_approvals_required) { 0 }

          it "returns an empty array" do
            expect(subject.codeowners_approvals).to be_empty
          end
        end

        context "one required approval is needed" do
          let(:number_of_approvals_required) { 1 }

          it "returns the required approval with a valid reviewer" do
            codeowners_approvals = subject.codeowners_approvals

            expect(codeowners_approvals.count).to eq(1)

            expect(codeowners_approvals.first.category).to eq(:Backend)
            expect(codeowners_approvals.first.spin.maintainer.username).to eq(backend_maintainer.username)
          end

          context "when there are no valid reviewers available" do
            let(:backend_available) { false }

            it "returns the required approval with a fallback reviewer" do
              codeowners_approvals = subject.codeowners_approvals

              expect(codeowners_approvals.count).to eq(1)
              expect(codeowners_approvals.first.spin.maintainer.username).to eq(valid_reviewer["username"]).or(invalid_reviewer["username"])
            end
          end

          context "when no one is in the right project" do
            let(:backend_maintainer_project) { {} }

            it "returns the required approval with a fallback reviewer" do
              codeowners_approvals = subject.codeowners_approvals

              expect(codeowners_approvals.count).to eq(1)
              expect(codeowners_approvals.first.spin.maintainer.username).to eq(valid_reviewer["username"]).or(invalid_reviewer["username"])
            end
          end
        end
      end

      context "when there are multiple approval rules" do
        let(:number_of_approvals_required) { 1 }
        let(:rule_for_ruby_files) do
          {
            "id" => 333,
            "name" => "*.rb",
            "rule_type" => "code_owner",
            "eligible_approvers" => [valid_reviewer_for_ruby_files],
            "approvals_required" => number_of_approvals_required,
            "section" => "Backend",
            "code_owner" => true
          }
        end

        let(:rule_for_rake_files) do
          {
            "id" => 551,
            "name" => "*.rake",
            "rule_type" => "code_owner",
            "eligible_approvers" => [valid_reviewer_for_rake_files],
            "approvals_required" => number_of_approvals_required,
            "section" => "Backend",
            "code_owner" => true
          }
        end

        let(:rule_for_js_files) do
          {
            "id" => 551,
            "name" => "*.js",
            "rule_type" => "code_owner",
            "eligible_approvers" => [valid_reviewer_for_js_files],
            "approvals_required" => number_of_approvals_required,
            "section" => "Frontend",
            "code_owner" => true
          }
        end

        let(:valid_reviewer_for_ruby_files) do
          {
            "id" => 123,
            "username" => backend_maintainer.username,
            "name" => backend_maintainer.name,
            "state" => "active"
          }
        end

        let(:valid_reviewer_for_rake_files) do
          {
            "id" => 456,
            "username" => another_backend_maintainer.username,
            "name" => another_backend_maintainer.name,
            "state" => "active"
          }
        end

        let(:valid_reviewer_for_js_files) do
          {
            "id" => 789,
            "username" => frontend_maintainer.username,
            "name" => frontend_maintainer.name,
            "state" => "active"
          }
        end

        context "when the same section has multiple approval rules" do
          let(:return_json) { { "rules" => [rule_for_ruby_files, rule_for_rake_files] } }

          context "and the rules require different approvers" do
            it "returns the required approvals from different approvers" do
              expect(subject.codeowners_approvals.count).to eq(2)
              expect(subject.codeowners_approvals[0].category).to eq(:Backend)
              expect(subject.codeowners_approvals[0].spin.maintainer.username).to eq(backend_maintainer.username)
              expect(subject.codeowners_approvals[1].category).to eq(:Backend)
              expect(subject.codeowners_approvals[1].spin.maintainer.username).to eq(another_backend_maintainer.username)
            end
          end

          context "and the rules share the same approvers" do
            let(:valid_reviewer_for_rake_files) do
              {
                "id" => 456,
                "username" => backend_maintainer.username,
                "name" => backend_maintainer.name,
                "state" => "active"
              }
            end

            it "returns just 1 approval from the 2 rules" do
              expect(subject.codeowners_approvals.count).to eq(1)
            end
          end
        end

        context "when approval rules include different sections" do
          let(:return_json) { { "rules" => [rule_for_ruby_files, rule_for_js_files] } }

          it "returns the required approvals from different section" do
            expect(subject.codeowners_approvals.count).to eq(2)
            expect(subject.codeowners_approvals[0].category).to eq(:Backend)
            expect(subject.codeowners_approvals[0].spin.maintainer.username).to eq(backend_maintainer.username)
            expect(subject.codeowners_approvals[1].category).to eq(:Frontend)
            expect(subject.codeowners_approvals[1].spin.maintainer.username).to eq(frontend_maintainer.username)
          end
        end

        context "when there is a generic codeowners rule" do
          let(:rule_for_generic_codeowners) do
            {
              "id" => 777,
              "name" => "*",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_js_files],
              "approvals_required" => 1,
              "section" => "codeowners",
              "code_owner" => true
            }
          end

          let(:return_json) { { "rules" => [rule_for_ruby_files, rule_for_generic_codeowners] } }

          it "returns the required approvals without the generic one" do
            expect(subject.codeowners_approvals.count).to eq(1)
            expect(subject.codeowners_approvals[0].category).to eq(:Backend)
            expect(subject.codeowners_approvals[0].spin.maintainer.username).to eq(backend_maintainer.username)
          end
        end

        context "when there is a codeowners rule without a section name" do
          let(:rule_for_codeowners_without_section) do
            {
              "id" => 777,
              "name" => "/path/or/glob/**/*.rb",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_ruby_files],
              "approvals_required" => 1,
              "section" => "codeowners",
              "code_owner" => true
            }
          end

          let(:return_json) { { "rules" => [rule_for_codeowners_without_section] } }

          it "returns the required approvals using name as the category" do
            expect(subject.codeowners_approvals.count).to eq(1)
            expect(subject.codeowners_approvals[0].category).to eq(:"`/path/or/glob/**/*.rb`")
            expect(subject.codeowners_approvals[0].spin.maintainer.username).to eq(backend_maintainer.username)
          end
        end

        context "when including an optional codeowners rule via config" do
          let(:number_of_approvals_required) { 0 }
          let(:rule_for_ruby_files) do
            {
              "id" => 333,
              "name" => "*.rb",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_ruby_files],
              "approvals_required" => number_of_approvals_required,
              "section" => "Backend",
              "code_owner" => true
            }
          end

          let(:return_json) { { "rules" => [rule_for_ruby_files] } }

          it "returns the appropriate approval rule" do
            allow(dangerfile.helper.config).to receive(:included_optional_codeowners_sections_for_roulette).and_return(["Backend"])
            expect(subject.codeowners_approvals.count).to eq(1)
            expect(subject.codeowners_approvals[0].category).to eq(:Backend)
          end
        end

        context "when excluding a required codeowners rule via config" do
          let(:number_of_approvals_required) { 1 }
          let(:rule_for_ruby_files) do
            {
              "id" => 333,
              "name" => "*.rb",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_ruby_files],
              "approvals_required" => number_of_approvals_required,
              "section" => "Backend",
              "code_owner" => true
            }
          end

          let(:return_json) { { "rules" => [rule_for_ruby_files] } }

          it "does not return the approval rule" do
            allow(dangerfile.helper.config).to receive(:excluded_required_codeowners_sections_for_roulette).and_return(["Backend"])
            expect(subject.codeowners_approvals.count).to eq(0)
          end
        end

        context "when including and excluding the same codeowners rule via config" do
          let(:number_of_approvals_required) { 1 }
          let(:rule_for_ruby_files) do
            {
              "id" => 333,
              "name" => "*.rb",
              "rule_type" => "code_owner",
              "eligible_approvers" => [valid_reviewer_for_ruby_files],
              "approvals_required" => number_of_approvals_required,
              "section" => "Backend",
              "code_owner" => true
            }
          end

          let(:return_json) { { "rules" => [rule_for_ruby_files] } }

          it "treats the rule as excluded" do
            allow(dangerfile.helper.config).to receive_messages(excluded_required_codeowners_sections_for_roulette: ["Backend"], included_optional_codeowners_sections_for_roulette: ["Backend"])
            expect(subject.codeowners_approvals.count).to eq(0)
          end
        end
      end
    end
  end
end

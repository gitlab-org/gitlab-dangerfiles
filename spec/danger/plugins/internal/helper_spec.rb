# frozen_string_literal: true

require "spec_helper"

RSpec.describe Danger::Helper do
  include_context "with dangerfile"

  it "is a plugin" do
    expect(described_class < Danger::Plugin).to be_truthy
  end

  let(:mr_author) { nil }
  let(:files_to_category) { { /\Aadded/ => [:doc], /\Amodified/ => [:changelog] } }
  let(:diff) { "- foo\n+ bar" }
  let(:all_changed_files) { %w[added1 added2 modified1 modified2 renamed.new] }
  let(:changes) do
    Gitlab::Dangerfiles::Changes.new([]).tap do |changes|
      %w[added1 added2 renamed.old].each do |file|
        changes << Gitlab::Dangerfiles::Change.new(file, :added)
      end

      %w[modified1 modified2].each do |file|
        changes << Gitlab::Dangerfiles::Change.new(file, :modified)
      end

      %w[deleted1].each do |file|
        changes << Gitlab::Dangerfiles::Change.new(file, :deleted)
      end

      changes << Gitlab::Dangerfiles::Change.new("renamed.old", :renamed_before)
      changes << Gitlab::Dangerfiles::Change.new("renamed.new", :renamed_after)
    end
  end
  let(:mr_changes_from_api) do
    {
      "changes" => [
        {
          "old_path" => "added-from-api",
          "new_path" => "added-from-api",
          "a_mode" => "100644",
          "b_mode" => "100644",
          "new_file" => true,
          "renamed_file" => false,
          "deleted_file" => false,
          "diff" => "@@ -49,6 +49,14 @@\n- vendor/ruby/\n     policy: pull\n \n+.danger-review-cache:\n"
        },
        {
          "old_path" => "modified-from-api",
          "new_path" => "modified-from-api",
          "a_mode" => "100644",
          "b_mode" => "100644",
          "new_file" => false,
          "renamed_file" => false,
          "deleted_file" => false,
          "diff" => "@@ -49,6 +49,14 @@\n- vendor/ruby/\n     policy: pull\n \n+.danger-review-cache:\n"
        },
        {
          "old_path" => "renamed_before-from-api",
          "new_path" => "renamed_after-from-api",
          "a_mode" => "100644",
          "b_mode" => "100644",
          "new_file" => false,
          "renamed_file" => true,
          "deleted_file" => false,
          "diff" => "@@ -49,6 +49,14 @@\n- vendor/ruby/\n     policy: pull\n \n+.danger-review-cache:\n"
        },
        {
          "old_path" => "deleted-from-api",
          "new_path" => "deleted-from-api",
          "a_mode" => "100644",
          "b_mode" => "100644",
          "new_file" => false,
          "renamed_file" => false,
          "deleted_file" => true,
          "diff" => "@@ -49,6 +49,14 @@\n- vendor/ruby/\n     policy: pull\n \n+.danger-review-cache:\n"
        },
      ]
    }
  end

  subject(:helper) { dangerfile.helper }

  describe "#config" do
    context "when running locally" do
      it "returns a Gitlab::Dangerfiles::Config object" do
        expect(helper.config).to be_an(Gitlab::Dangerfiles::Config)
      end
    end

    it "is possible to override the config with a block" do
      new_config = {
        high: helper.config.code_size_thresholds[:high] / 2,
        medium: helper.config.code_size_thresholds[:medium] / 2
      }

      helper.config do |config|
        config.code_size_thresholds = new_config
      end

      expect(helper.config.code_size_thresholds).to eq(new_config)
    end
  end

  describe "#html_link" do
    let(:text) { "something" }

    context "when running locally" do
      before do
        allow(helper).to receive(:ci?).and_return(false)
      end

      it "returns the same string" do
        expect(dangerfile.gitlab).not_to receive(:html_link)

        expect(helper.html_link(text)).to eq(text)
      end
    end

    context "when running under CI" do
      it "returns a HTML link formatted version of the string" do
        html_formatted_text = %Q{<a href="#{text}">#{text}</a>}

        expect(dangerfile.gitlab).to receive(:html_link).with(text, full_path: true).and_return(html_formatted_text)

        expect(helper.html_link(text)).to eq(html_formatted_text)
      end
    end
  end

  describe "#ci?" do
    context "when gitlab_danger_helper is not available" do
      before do
        allow(helper).to receive(:gitlab_helper).and_return(nil)
      end

      it "returns false" do
        expect(helper.ci?).to be_falsey
      end
    end

    context "when gitlab_danger_helper is available" do
      it "returns true" do
        expect(helper.ci?).to be_truthy
      end
    end
  end

  describe "#gitlab_helper" do
    it "returns the gitlab helper" do
      expect(helper.__send__(:gitlab_helper)).to eq(dangerfile.gitlab)
    end
  end

  describe "#release_automation?" do
    before do
      allow(dangerfile.gitlab).to receive(:mr_author).and_return(mr_author)
    end

    context "when gitlab helper is not available" do
      it "returns false" do
        expect(helper.release_automation?).to be_falsey
      end
    end

    context "when gitlab helper is available" do
      context "but the MR author isn't the RELEASE_TOOLS_BOT" do
        let(:mr_author) { "johnmarston" }

        it "returns false" do
          expect(helper.release_automation?).to be_falsey
        end
      end

      context "and the MR author is the RELEASE_TOOLS_BOT" do
        let(:mr_author) { described_class::RELEASE_TOOLS_BOT }

        it "returns true" do
          expect(helper.release_automation?).to be_truthy
        end
      end
    end
  end

  describe "#added_files" do
    subject { helper.added_files }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to contain_exactly("added-from-api")
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "interprets a list of changes from the danger git plugin" do
        is_expected.to contain_exactly("added-from-git")
      end
    end
  end

  describe "#modified_files" do
    subject { helper.modified_files }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to contain_exactly("modified-from-api")
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "interprets a list of changes from the danger git plugin" do
        is_expected.to contain_exactly("modified-from-git")
      end
    end
  end

  describe "#renamed_files" do
    subject { helper.renamed_files }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to eq([{ before: "renamed_before-from-api", after: "renamed_after-from-api" }])
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to eq([{ before: "renamed_before-from-git", after: "renamed_after-from-git" }])
      end
    end
  end

  describe "#deleted_files" do
    subject { helper.deleted_files }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to contain_exactly("deleted-from-api")
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "interprets a list of changes from the danger git plugin" do
        is_expected.to contain_exactly("deleted-from-git")
      end
    end
  end

  describe "#all_changed_files" do
    subject { helper.all_changed_files }

    before do
      allow(helper).to receive(:changes).and_return(changes)
    end

    it "interprets a list of changes from the danger git plugin" do
      is_expected.to match_array(all_changed_files)
    end
  end

  describe "#changed_lines" do
    subject { helper.changed_lines("added-from-api") }

    context "when running locally" do
      before do
        allow(helper).to receive(:ci?).and_return(false)
        allow(helper).to receive(:diff_for_file).with("added-from-api").and_return(diff)
      end

      it "returns diff from Git" do
        is_expected.to eq(["- foo", "+ bar"])
      end
    end

    context "when running under CI" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "returns diff from the API" do
        is_expected.to eq(["- vendor/ruby/", "+.danger-review-cache:"])
      end
    end
  end

  describe "#markdown_list" do
    it "creates a markdown list of items" do
      items = %w[a b]

      expect(helper.markdown_list(items)).to eq("* `a`\n* `b`")
    end

    it "wraps items in <details> when there are more than 10 items" do
      items = ("a".."k").to_a

      expect(helper.markdown_list(items)).to match(%r{<details>[^<]+</details>})
    end
  end

  describe "#changes_by_category" do
    before do
      allow(helper).to receive(:changes).and_return(changes)
    end

    it "categorizes changed files" do
      expect(helper.changes_by_category(files_to_category)).to eq(doc: %w[added1 added2], changelog: %w[modified1 modified2], none: ["renamed.new"])
    end

    context "with [] categories mapping" do
      it "returns files as :none category" do
        expect(helper.changes_by_category([])).to eq(none: %w[added1 added2 modified1 modified2 renamed.new])
      end
    end

    context "with nil categories mapping" do
      it "returns files as :none category" do
        expect(helper.changes_by_category(nil)).to eq(none: %w[added1 added2 modified1 modified2 renamed.new])
      end
    end

    context "with no categories mapping" do
      it "returns files as :none category" do
        expect(helper.changes_by_category).to eq(none: %w[added1 added2 modified1 modified2 renamed.new])
      end
    end

    context "with helper.config.files_to_category set" do
      before do
        helper.config.files_to_category = files_to_category
      end

      it "categorizes changed files" do
        expect(helper.changes_by_category(files_to_category)).to eq(doc: %w[added1 added2], changelog: %w[modified1 modified2], none: ["renamed.new"])
      end
    end
  end

  describe "#changes" do
    before do
      allow(helper).to receive(:changes).and_call_original
      allow(helper).to receive_messages(added_files: %w[added1 added2 renamed.old], modified_files: %w[modified1 modified2], deleted_files: %w[deleted1])
      allow(helper).to receive(:renamed_files).twice.and_return([{ before: "renamed.old", after: "renamed.new" }])
    end

    it "returns an array of Change objects" do
      expect(helper.changes(files_to_category)).to be_an(Gitlab::Dangerfiles::Changes)
    end

    it "groups changes by change type" do
      changes = helper.changes(files_to_category)

      expect(changes.added.files).to contain_exactly("added1", "added2", "renamed.old")
      expect(changes.modified.files).to contain_exactly("modified1", "modified2")
      expect(changes.deleted.files).to contain_exactly("deleted1")
      expect(changes.renamed_before.files).to contain_exactly("renamed.old")
      expect(changes.renamed_after.files).to contain_exactly("renamed.new")
    end

    context "with [] categories mapping" do
      it "raises no error" do
        expect(helper.changes([])).to be_an(Gitlab::Dangerfiles::Changes)
      end
    end

    context "with nil categories mapping" do
      it "raises no error" do
        expect(helper.changes(nil)).to be_an(Gitlab::Dangerfiles::Changes)
      end
    end

    context "with no categories mapping" do
      it "raises no error" do
        expect(helper.changes).to be_an(Gitlab::Dangerfiles::Changes)
      end
    end

    context "with multiple categories listed for a file" do
      let(:files_to_category) { { [/\A[added]/] => [:doc], /\Amodified1/ => [:number_1, :changelog_1] } }

      it "lists every file only once" do
        expect(helper.changes(files_to_category).files).to contain_exactly("added1", "added2", "modified1", "modified2", "deleted1", "renamed.old", "renamed.new")
      end
    end
  end

  describe "#categories_for_file" do
    context "with categories mapping given" do
      using RSpec::Parameterized::TableSyntax

      where(:path, :expected_categories) do
        "added1" | [:doc]
        "added2" | [:doc]
        "modified1" | [:changelog]
        "modified2" | [:changelog]
        "renamed.new" | [:none]
      end

      with_them do
        subject { helper.categories_for_file(path, files_to_category) }

        it { is_expected.to eq(expected_categories) }
      end
    end

    context "with [] categories mapping" do
      it "raises no error" do
        expect(helper.categories_for_file("doc/foo", [])).to eq([:none])
      end
    end

    context "with nil categories mapping" do
      it "raises no error" do
        expect(helper.categories_for_file("doc/foo", nil)).to eq([:none])
      end
    end

    context "with no categories mapping" do
      it "raises no error" do
        expect(helper.categories_for_file("doc/foo")).to eq([:none])
      end
    end
  end

  describe "#label_for_category" do
    using RSpec::Parameterized::TableSyntax

    where(:category, :expected_label) do
      :backend | '~"backend"'
      :database | '~"database"'
      :docs | "~documentation"
      :foo | '~"foo"'
      :frontend | '~"frontend"'
      :none | "None"
      nil | "N/A"
      :qa | "~QA"
      :ux | "~UX"
      :codeowners | '~"Code Owners"'
      :test | "~test for `spec/features/*`"
      :"`/path/to/somewhere`" | "`/path/to/somewhere`"
      :tooling | '~"maintenance::workflow" for tooling, Danger, and RuboCop'
      :pipeline | '~"maintenance::pipelines" for CI'
      :ci_template | '~"ci::templates"'
      :analytics_instrumentation | '~"analytics instrumentation"'
      :import_integrate_be | '~"group::import and integrate" (backend)'
      :import_integrate_fe | '~"group::import and integrate" (frontend)'
      :Authentication | '~"group::authentication"'
      :Authorization | '~"group::authorization"'
      :Compliance | '~"group::compliance"'
    end

    with_them do
      subject { helper.label_for_category(category) }

      it { is_expected.to eq(expected_label) }
    end
  end

  describe "#mr_source_project_id" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_source_project_id).to eq("")
    end

    it "returns the MR IID when `gitlab_helper` is available" do
      mr_source_project_id = 1234
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("source_project_id" => mr_source_project_id)

      expect(helper.mr_source_project_id).to eq(mr_source_project_id.to_s)
    end
  end

  describe "#mr_target_project_id" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_target_project_id).to eq("")
    end

    it "returns the MR IID when `gitlab_helper` is available" do
      mr_target_project_id = 1234
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_project_id" => mr_target_project_id)

      expect(helper.mr_target_project_id).to eq(mr_target_project_id.to_s)
    end
  end

  describe "#mr_iid" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_iid).to eq("")
    end

    it "returns the MR IID when `gitlab_helper` is available" do
      mr_iid = 1234
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("iid" => mr_iid)

      expect(helper.mr_iid).to eq(mr_iid.to_s)
    end
  end

  describe "#mr_author" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_author).to eq(`whoami`.strip)
    end

    it "returns the MR author when `gitlab_helper` is available" do
      mr_author = "1234"
      expect(dangerfile.gitlab).to receive(:mr_author).and_return(mr_author)

      expect(helper.mr_author).to eq(mr_author)
    end
  end

  describe "#mr_assignees" do
    it "returns [] when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_assignees).to eq([])
    end

    it "returns the MR assignees when `gitlab_helper` is available" do
      mr_assignees = [{ username: "joe" }]
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("assignees" => mr_assignees)

      expect(helper.mr_assignees).to eq(mr_assignees)
    end
  end

  describe "#mr_title" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_title).to eq("")
    end

    it "returns the MR title when `gitlab_helper` is available" do
      mr_title = "My MR title"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => mr_title)

      expect(helper.mr_title).to eq(mr_title)
    end
  end

  describe "#mr_description" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_description).to eq("")
    end

    it "returns the MR description when `gitlab_helper` is available" do
      mr_description = "My MR description"
      expect(dangerfile.gitlab).to receive(:mr_body).and_return(mr_description)

      expect(helper.mr_description).to eq(mr_description)
    end
  end

  describe "#mr_web_url" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_web_url).to eq("")
    end

    it "returns the MR web_url when `gitlab_helper` is available" do
      mr_web_url = "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("web_url" => mr_web_url)

      expect(helper.mr_web_url).to eq(mr_web_url)
    end
  end

  describe "#mr_milestone" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_milestone).to be_nil
    end

    it "returns the MR milestone when `gitlab_helper` is available" do
      mr_milestone = { title: "hello" }
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("milestone" => mr_milestone)

      expect(helper.mr_milestone).to eq(mr_milestone)
    end
  end

  describe "#mr_labels" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_labels).to eq([])
    end

    it "returns the MR labels when `gitlab_helper` is available" do
      mr_labels = %w[foo bar baz]
      expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)

      expect(helper.mr_labels).to eq(mr_labels)
    end

    context "when labels_to_add includes some labels" do
      it "returns the MR labels + labels_to_add" do
        helper.labels_to_add << "a" << "foo"

        mr_labels = %w[foo bar baz]
        expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)

        expect(helper.mr_labels).to eq(mr_labels + ["a"])
      end
    end
  end

  describe "#mr_source_branch" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_source_branch).to eq(`git rev-parse --abbrev-ref HEAD`.strip)
    end

    it "returns the MR source branch when `gitlab_helper` is available" do
      mr_source_branch = "main"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("source_branch" => mr_source_branch)

      expect(helper.mr_source_branch).to eq(mr_source_branch)
    end
  end

  describe "#mr_target_branch" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_target_branch).to eq("")
    end

    it "returns the MR target branch when `gitlab_helper` is available" do
      mr_target_branch = "main"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => mr_target_branch)

      expect(helper.mr_target_branch).to eq(mr_target_branch)
    end
  end

  describe "#mr_approval_state" do
    it "returns {} when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_approval_state).to eq({})
    end

    it "returns the MR approval state when `gitlab_helper` is available" do
      mr_target_project_id = 135
      mr_iid = 246
      mr_approval_state = { "rules" => [] }

      expect(helper).to receive(:mr_target_project_id).and_return(mr_target_project_id)
      expect(helper).to receive(:mr_iid).and_return(mr_iid)
      expect(dangerfile.gitlab.api).to receive(:merge_request_approval_state).with(mr_target_project_id, mr_iid).and_return(mr_approval_state)

      expect(helper.mr_approval_state).to eq(mr_approval_state)
    end
  end

  describe "#revert_mr?" do
    context "when `gitlab_helper` is unavailable" do
      before do
        allow(helper).to receive(:gitlab_helper).and_return(nil)
        allow(helper.git).to receive(:commits).and_return(commits)
      end

      context 'with a single commit' do
        using RSpec::Parameterized::TableSyntax

        let(:commits) { [double(:commit, message: message)] }

        where(:message, :result) do
          'Revert "me"'    | true
          'revert "me"'    | true
          ' Revert "me"'   | false
          'Revertme'       | false
          'Something else' | false
        end

        with_them do
          if params[:result]
            it { expect(helper).to be_revert_mr }
          else
            it { expect(helper).not_to be_revert_mr }
          end
        end
      end

      context 'without commits' do
        let(:commits) { [] }

        it { expect(helper).not_to be_revert_mr }
      end

      context 'with multiple commits' do
        let(:commits) do
          [
            double(:commit, message: "Revert 'foo'"),
            double(:commit, message: "Revert 'bar'")
          ]
        end

        it { expect(helper).not_to be_revert_mr }
      end
    end

    context "when MR has its title starting with `revert`" do
      before do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "revert - A revert MR")
      end

      it { expect(helper).to be_revert_mr }
    end

    context "when MR has its title starting with `Revert`" do
      before do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Revert - Another revert MR")
      end

      it { expect(helper).to be_revert_mr }
    end

    context "when MR title does not start with `revert` or `Revert`" do
      before do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "revertAn MR without space")
      end

      it { expect(helper).not_to be_revert_mr }
    end
  end

  describe "#squash_mr?" do
    it "returns true when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper).to be_squash_mr
    end

    it "returns true when MR is to set to be squashed" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("squash" => true)

      expect(helper).to be_squash_mr
    end
    it "returns true when MR is to set to be squashed" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("squash" => false)

      expect(helper).not_to be_squash_mr
    end
  end

  describe "#security_mr?" do
    it "returns false when on a normal merge request" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("web_url" => "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1")

      expect(helper).not_to be_security_mr
    end

    it "returns true when on a security merge request" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("web_url" => "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1")

      expect(helper).to be_security_mr
    end
  end

  describe "#stable_branch_mr?" do
    it "returns false when on the default branch" do
      mr_target_branch = "main"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => mr_target_branch)

      expect(helper.stable_branch_mr?).to be(false)
    end

    it "returns true when on a stable branch" do
      mr_target_branch = "15-10-stable-ee"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => mr_target_branch)
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("web_url" => "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1")

      expect(helper.stable_branch_mr?).to be(true)
    end

    it "returns false when on a stable branch on a security MR" do
      mr_target_branch = "15-10-stable-ee"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => mr_target_branch)
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("web_url" => "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1")

      expect(helper.stable_branch_mr?).to be(false)
    end
  end

  describe "#draft_mr?" do
    it "returns true for a draft MR" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("work_in_progress" => true)

      expect(helper).to be_draft_mr
    end

    it "returns false for non draft MR" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("work_in_progress" => false)

      expect(helper).not_to be_draft_mr
    end
  end

  describe "#cherry_pick_mr?" do
    context "when MR title does not mention a cherry-pick" do
      it "returns false" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz")

        expect(helper).not_to be_cherry_pick_mr
      end
    end

    context "when MR title mentions a cherry-pick" do
      [
        "Cherry Pick !1234",
        "cherry-pick !1234",
        "CherryPick !1234",
      ].each do |mr_title|
        it "returns true" do
          expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => mr_title)

          expect(helper).to be_cherry_pick_mr
        end
      end
    end
  end

  describe "#run_all_rspec_mr?" do
    context "when MR title does not mention RUN ALL RSPEC" do
      it "returns false" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz")

        expect(helper).not_to be_run_all_rspec_mr
      end
    end

    context "when MR title mentions RUN ALL RSPEC" do
      it "returns true" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz RUN ALL RSPEC")

        expect(helper).to be_run_all_rspec_mr
      end
    end
  end

  describe "#run_as_if_foss_mr?" do
    context "when MR title does not mention RUN AS-IF-FOSS" do
      it "returns false" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz")

        expect(helper).not_to be_run_as_if_foss_mr
      end
    end

    context "when MR title mentions RUN AS-IF-FOSS" do
      it "returns true" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz RUN AS-IF-FOSS")

        expect(helper).to be_run_as_if_foss_mr
      end
    end
  end

  describe "#stable_branch?" do
    it "returns false when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper).not_to be_stable_branch
    end

    context "when MR target branch is not a stable branch" do
      it "returns false" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => "my-feature-branch")

        expect(helper).not_to be_stable_branch
      end
    end

    context "when MR target branch is a stable branch" do
      %w[
        13-1-stable-ee
        13-1-stable-ee-patch-1
      ].each do |target_branch|
        it "returns true" do
          expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => target_branch)

          expect(helper).to be_stable_branch
        end
      end
    end
  end

  describe "#has_scoped_label_with_scope?" do
    it "returns false when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.has_scoped_label_with_scope?("type")).to be_falsey
    end

    context "when mr has labels" do
      before do
        mr_labels = ["foo", "type::feature"]
        expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)
      end

      it "returns true with a matched label" do
        expect(helper.has_scoped_label_with_scope?("type")).to be_truthy
      end

      it "returns false with unmatched label" do
        expect(helper.has_scoped_label_with_scope?("database")).to be_falsey
      end
    end
  end

  describe "#mr_has_labels?" do
    it "returns false when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_has_labels?("telemetry")).to be_falsey
    end

    context "when mr has labels" do
      before do
        mr_labels = ["telemetry", "telemetry::reviewed"]
        expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)
      end

      it "returns true with a matched label" do
        expect(helper.mr_has_labels?("telemetry")).to be_truthy
      end

      it "returns false with unmatched label" do
        expect(helper.mr_has_labels?("database")).to be_falsey
      end

      it "returns true with an array of labels" do
        expect(helper.mr_has_labels?(["telemetry", "telemetry::reviewed"])).to be_truthy
      end

      it "returns true with multi arguments with matched labels" do
        expect(helper.mr_has_labels?("telemetry", "telemetry::reviewed")).to be_truthy
      end

      it "returns false with multi arguments with unmatched labels" do
        expect(helper.mr_has_labels?("telemetry", "telemetry::non existing")).to be_falsey
      end
    end
  end

  describe "#labels_list" do
    let(:labels) { ["telemetry", "telemetry::reviewed"] }

    it "composes the labels string" do
      expect(helper.labels_list(labels)).to eq('~"telemetry", ~"telemetry::reviewed"')
    end

    context "when passing a separator" do
      it "composes the labels string with the given separator" do
        expect(helper.labels_list(labels, sep: " ")).to eq('~"telemetry" ~"telemetry::reviewed"')
      end
    end

    it "returns empty string for empty array" do
      expect(helper.labels_list([])).to eq("")
    end
  end

  describe "#quick_action_label" do
    it "composes the labels string" do
      mr_labels = ["telemetry", "telemetry::reviewed"]

      expect(helper.quick_action_label(mr_labels)).to eq('/label ~"telemetry" ~"telemetry::reviewed"')
    end

    it "returns empty string for empty array" do
      expect(helper.quick_action_label([])).to eq("")
    end
  end

  describe "#has_ci_changes?" do
    context "when .gitlab/ci is changed" do
      it "returns true" do
        expect(helper).to receive(:all_changed_files).and_return(%w[migration.rb .gitlab/ci/test.yml])

        expect(helper.has_ci_changes?).to be_truthy
      end
    end

    context "when .gitlab-ci.yml is changed" do
      it "returns true" do
        expect(helper).to receive(:all_changed_files).and_return(%w[migration.rb .gitlab-ci.yml])

        expect(helper.has_ci_changes?).to be_truthy
      end
    end

    context "when neither .gitlab/ci/ or .gitlab-ci.yml is changed" do
      it "returns false" do
        expect(helper).to receive(:all_changed_files).and_return(%w[migration.rb nested/.gitlab-ci.yml])

        expect(helper.has_ci_changes?).to be_falsey
      end
    end
  end

  describe "#group_label" do
    before do
      expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)
    end

    context "when no group label is present" do
      let(:mr_labels) { %w[foo bar] }

      it "returns nil" do
        expect(helper.group_label).to be_nil
      end
    end

    context "when a group label is present" do
      let(:mr_labels) { ["foo", "group::source code", "bar"] }

      it "returns the group label" do
        expect(helper.group_label).to eq("group::source code")
      end
    end
  end

  describe "#stage_label" do
    before do
      expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)
    end

    context "when no stage label is present" do
      let(:mr_labels) { %w[foo bar] }

      it "returns nil" do
        expect(helper.stage_label).to be_nil
      end
    end

    context "when a stage label is present" do
      let(:mr_labels) { ["foo", "devops::create", "bar"] }

      it "returns the stage label" do
        expect(helper.stage_label).to eq("devops::create")
      end
    end
  end

  describe "#labels_to_add" do
    context "when there's no label to add yet" do
      it "returns []" do
        expect(helper.labels_to_add).to be_empty
      end
    end

    context "when there are labels to add" do
      it "returns the labels to add" do
        helper.labels_to_add << "a" << "a" << "b"

        expect(helper.labels_to_add).to match_array(%w[a a b])
      end
    end
  end

  describe "#current_milestone" do
    context "with `gitlab_helper` available" do
      before do
        WebMock
          .stub_request(:get, "https://gitlab.com/api/v4/groups/9970/milestones?state=active")
          .to_return(body: milestones.to_json)
      end

      context "with no numeric milestones" do
        let(:milestones) do
          [
            {
              title: "ClickHouse Acceleration",
              state: "active",
              due_date: "2022-06-22",
              expired: false,
              start_date: "2022-04-06"
            },
          ]
        end

        it "returns no milestones" do
          expect(helper.current_milestone).to be_nil
        end
      end

      context "with only expired milestones" do
        let(:milestones) do
          [
            {
              title: "16.0",
              state: "active",
              due_date: "2022-06-22",
              expired: true,
              start_date: "2022-04-06"
            },
          ]
        end

        it "returns no milestones" do
          expect(helper.current_milestone).to be_nil
        end
      end

      context "with milestones without a start date" do
        let(:milestones) do
          [
            {
              title: "16.0",
              state: "active",
              due_date: "2022-06-22",
              expired: false,
              start_date: nil
            },
          ]
        end

        it "returns no milestones" do
          expect(helper.current_milestone).to be_nil
        end
      end

      context "with milestones without a due date" do
        let(:milestones) do
          [
            {
              title: "16.0",
              state: "active",
              due_date: nil,
              expired: false,
              start_date: "2022-06-22"
            },
          ]
        end

        it "returns no milestones" do
          expect(helper.current_milestone).to be_nil
        end
      end

      context "with valid milestones" do
        let(:milestones) do
          [
            {
              title: "15.1",
              state: "active",
              due_date: "2022-05-22",
              expired: false,
              start_date: "2022-04-22"
            },
            {
              title: "15.0",
              state: "active",
              due_date: "2022-04-22",
              expired: false,
              start_date: "2022-03-22"
            },
          ]
        end

        it "returns the milestone with the earliest start_date" do
          expect(helper.current_milestone.title).to eq("15.0")
        end
      end

      context "with current milestone in the 2nd page" do
        let(:milestones) do
          [
            {
              title: "15.1",
              state: "active",
              due_date: "2022-05-22",
              expired: false,
              start_date: "2022-04-22"
            },
          ]
        end

        let(:milestones_2nd_page) do
          [
            {
              title: "15.0",
              state: "active",
              due_date: "2022-04-22",
              expired: false,
              start_date: "2022-03-22"
            },
          ]
        end

        before do
          WebMock
            .stub_request(:get, "https://gitlab.com/api/v4/groups/9970/milestones?state=active")
            .to_return(
              body: milestones.to_json,
              headers: {
                "Link" => "<https://gitlab.com/api/v4/groups/9970/milestones?state=active&page=2>; rel=\"next\""
              }
            )

          WebMock
            .stub_request(:get, "https://gitlab.com/api/v4/groups/9970/milestones?state=active&page=2")
            .to_return(body: milestones_2nd_page.to_json)
        end

        it "returns the milestone with the earliest start_date" do
          expect(helper.current_milestone.title).to eq("15.0")
        end
      end
    end

    context "with `gitlab_helper` unavailable" do
      before do
        expect(helper).to receive(:gitlab_helper).and_return(nil)
      end

      it "returns nil" do
        expect(helper.current_milestone).to be_nil
      end
    end
  end

  # Private methods

  describe "#diff_for_file" do
    subject { helper.__send__(:diff_for_file, "added-from-api") }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "returns the changes for the given file from the GitLab API" do
        is_expected.to eq(mr_changes_from_api["changes"][0]["diff"])
      end

      context "when the given file isn't modified in the API" do
        subject { helper.__send__(:diff_for_file, "not-added-from-api") }

        it "returns nil" do
          is_expected.to be_nil
        end
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "returns nil" do
        expect(fake_git).to receive(:diff_for_file).with("added-from-api")
        is_expected.to be_nil
      end

      context "when the file is modified in Git" do
        let(:fake_git_diff) { double(patch: diff) }

        subject { helper.__send__(:diff_for_file, "added-from-git") }

        it "returns the changes for the given file from the danger git plugin" do
          expect(fake_git).to receive(:diff_for_file).with("added-from-git").and_return(fake_git_diff)
          is_expected.to eq(diff)
        end
      end
    end
  end

  describe "#changes_from_api" do
    context "when running locally" do
      before do
        allow(helper).to receive(:ci?).and_return(false)
      end

      it "returns nil" do
        expect(helper).not_to receive(:gitlab_helper)
        expect(helper.__send__(:changes_from_api)).to be_nil
      end
    end

    context "when running under CI" do
      it "returns changes from the API" do
        expect(dangerfile.gitlab).to receive(:mr_changes).and_return(mr_changes_from_api["changes"])

        expect(helper.__send__(:changes_from_api)).to eq(mr_changes_from_api["changes"])
      end

      context "when the API raises an error" do
        it "returns nil" do
          expect(dangerfile.gitlab).to receive(:mr_changes).and_raise
          expect(helper.__send__(:changes_from_api)).to be_nil
        end
      end
    end
  end
end

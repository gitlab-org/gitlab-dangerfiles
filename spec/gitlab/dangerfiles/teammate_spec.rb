# frozen_string_literal: true

require "spec_helper"

require "gitlab/dangerfiles/teammate"

RSpec.describe Gitlab::Dangerfiles::Teammate do
  using RSpec::Parameterized::TableSyntax

  include_context "with teammates"

  subject { described_class.new(options) }

  let(:tz_offset_hours) { 2.0 }
  let(:options) do
    {
      "username" => "luigi",
      "name" => "Luigi",
      "projects" => projects,
      "role" => role,
      "specialty" => specialty,
      "markdown_name" => "@luigi",
      "tz_offset_hours" => tz_offset_hours
    }
  end

  let(:category) { :backend }
  let(:kind) { :reviewer }
  let(:capabilities) { ["#{kind} #{category}"] }
  let(:projects) { { project => capabilities } }
  let(:role) { "Engineer, Manage" }
  let(:specialty) { nil }
  let(:labels) { [] }
  let(:project) { "gitlab" }

  describe ".has_member_for_the_group?" do
    subject do
      described_class.has_member_for_the_group?(
        category, labels: labels, project: project)
    end

    let(:teammates) { [options] }
    let(:labels) { ["group::source code"] }
    let(:specialty) { "Create: Source Code" }

    context "when there is a reviewer for the group" do
      it "returns true" do
        expect(subject).to eq(true)
      end
    end

    context "when there is a maintainer for the group" do
      let(:kind) { :maintainer }

      it "returns true" do
        expect(subject).to eq(true)
      end
    end

    context "when there is a traintainer for the group" do
      let(:kind) { :traintainer }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "when there is a reviewer for the group but in a different category" do
      let(:capabilities) { ["reviewer frontend"] }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "when there is no one for the group" do
      let(:specialty) { "Create: Editor" }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "when there is no one for project" do
      let(:projects) { { "another-gitlab" => capabilities } }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end
  end

  describe ".fetch_company_members" do
    context "with HTTP failure" do
      before do
        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return(status: [431, "Request Header Fields Too Large"])
      end

      it "warns and return an empty list" do
        expect(described_class.fetch_company_members).to eq([])
        expect(described_class.warnings).to include("HTTPError: Failed to read #{described_class::ROULETTE_DATA_URL}: 431 Request Header Fields Too Larg.")
      end
    end

    context "with JSON failure" do
      before do
        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return(body: "INVALID JSON")
      end

      it "warns and return an empty list" do
        expect(described_class.fetch_company_members).to eq([])
        expect(described_class.warnings).to include(/Failed to parse/)
      end
    end

    context "when success" do
      before do
        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return(body: teammate_json)
      end

      it "returns an array of teammates" do
        expect(described_class.fetch_company_members).to match_teammates([
          backend_maintainer,
          frontend_reviewer,
          frontend_maintainer,
          software_engineer_in_test,
          tooling_reviewer,
          ci_template_reviewer
        ])
        expect(described_class.warnings).to eq([])
      end
    end

    context "when redirected" do
      let(:location) { "http://example.com?query" }
      let(:clean_location) { "http://example.com" }

      before do
        WebMock
          .stub_request(:get, described_class::ROULETTE_DATA_URL)
          .to_return({ status: 302, headers: { location: location } }, { body: teammate_json })
      end

      it "warns and return an empty list" do
        expect(described_class.fetch_company_members).to eq([])
        expect(described_class.warnings).to include("Redirection detected: #{clean_location}.")
      end
    end
  end

  describe ".company_members" do
    it "memoizes the result" do
      members_first = described_class.company_members.object_id
      members_second = described_class.company_members.object_id

      expect(members_first).to eq(members_second)
    end
  end

  describe "#member_of_the_group?" do
    subject(:teammate) { described_class.new(options) }

    context "when the specialty matches the group label" do
      let(:labels) { ["group::source code"] }
      let(:specialty) { "Create: Source Code" }

      it "returns true" do
        expect(teammate.member_of_the_group?(labels)).to eq(true)
      end
    end

    context "when the specialty does not match the group label" do
      let(:labels) { ["group::editor"] }
      let(:specialty) { "Create: Source Code" }

      it "returns false" do
        expect(teammate.member_of_the_group?(labels)).to eq(false)
      end
    end
  end

  describe "#==" do
    it "compares Teammate username" do
      joe1 = described_class.new("username" => "joe", "projects" => projects)
      joe2 = described_class.new("username" => "joe", "projects" => [])
      jane1 = described_class.new("username" => "jane", "projects" => projects)
      jane2 = described_class.new("username" => "jane", "projects" => [])

      expect(joe1).to eq(joe2)
      expect(jane1).to eq(jane2)
      expect(jane1).not_to eq(nil)
      expect(described_class.new("username" => nil)).not_to eq(nil)
    end
  end

  describe "#to_h" do
    it "returns the given options" do
      expect(subject.to_h).to eq(options)
    end
  end

  context "when having multiple capabilities" do
    let(:capabilities) { ["reviewer backend", "maintainer frontend", "trainee_maintainer qa", "reviewer ux"] }

    it "#reviewer? supports multiple roles per project" do
      expect(subject.reviewer?(project, :backend, labels)).to be_truthy
    end

    it "#reviewer? supports multiple roles per project" do
      allow(subject).to receive(:member_of_the_group?).and_return(true)

      expect(subject.reviewer?(project, :ux, labels)).to be_truthy
    end

    it "#traintainer? supports multiple roles per project" do
      expect(subject.traintainer?(project, :qa, labels)).to be_truthy
    end

    it "#maintainer? supports multiple roles per project" do
      expect(subject.maintainer?(project, :frontend, labels)).to be_truthy
    end

    context "when labels contain ~\"group::source code\"" do
      let(:labels) { ["group::source code"] }

      context "when the contribution is from wider community" do
        before do
          labels << "Community contribution"
        end

        context "when specialty contains Create:Source Code" do
          let(:specialty) { "Create:Source Code" }

          it "#reviewer? returns true" do
            expect(subject.reviewer?(project, :ux, labels)).to be_truthy
          end
        end

        context "when specialty contains, Configure" do
          let(:specialty) { "Configure" }

          it "#reviewer? returns false" do
            expect(subject.reviewer?(project, :ux, labels)).to be_falsey
          end
        end
      end
    end

    context "when labels contain devops::create" do
      let(:labels) { ["devops::create"] }

      context "when category is test and role is Software Engineer in Test, Create" do
        let(:role) { "Software Engineer in Test, Create" }

        it "#reviewer? returns true" do
          expect(subject.reviewer?(project, :test, labels)).to be_truthy
        end

        it "#maintainer? returns false" do
          expect(subject.maintainer?(project, :test, labels)).to be_falsey
        end

        context "when hyperlink is mangled in the role" do
          let(:role) { '<a href="#">Software Engineer in Test</a>, Create' }

          it "#reviewer? returns true" do
            expect(subject.reviewer?(project, :test, labels)).to be_truthy
          end
        end
      end
    end

    context "when role is Software Engineer in Test" do
      let(:role) { "Software Engineer in Test" }

      it "#reviewer? returns false" do
        expect(subject.reviewer?(project, :test, labels)).to be_falsey
      end
    end

    context "when role is Software Engineer in Test, Manage" do
      let(:role) { "Software Engineer in Test, Manage" }

      it "#reviewer? returns false" do
        expect(subject.reviewer?(project, :test, labels)).to be_falsey
      end
    end

    context "when capabilities include 'reviewer tooling'" do
      let(:capabilities) { ["reviewer tooling"] }

      it "#reviewer? returns true" do
        expect(subject.reviewer?(project, :tooling, labels)).to be_truthy
      end

      it "#maintainer? returns false" do
        expect(subject.maintainer?(project, :tooling, labels)).to be_falsey
      end
    end

    context "when capabilities include 'maintainer tooling'" do
      let(:capabilities) { ["maintainer tooling"] }

      it "#reviewer? returns false" do
        expect(subject.reviewer?(project, :tooling, labels)).to be_falsey
      end

      it "#maintainer? returns true" do
        expect(subject.maintainer?(project, :tooling, labels)).to be_truthy
      end
    end

    context "when capabilities include 'reviewer backend'" do
      let(:capabilities) { ["reviewer backend"] }

      it "#reviewer? returns true" do
        expect(subject.reviewer?(project, :tooling, labels)).to be_truthy
      end

      it "#maintainer? returns false" do
        expect(subject.maintainer?(project, :tooling, labels)).to be_falsey
      end
    end

    context "when capabilities include 'maintainer backend'" do
      let(:capabilities) { ["maintainer backend"] }

      it "#reviewer? returns false" do
        expect(subject.reviewer?(project, :tooling, labels)).to be_falsey
      end

      it "#maintainer? returns false" do
        expect(subject.maintainer?(project, :tooling, labels)).to be_falsey
      end
    end

    context "when capabilities include 'trainee_maintainer backend'" do
      let(:capabilities) { ["trainee_maintainer backend"] }

      it "#traintainer? returns false" do
        expect(subject.traintainer?(project, :tooling, labels)).to be_falsey
      end
    end

    context "when role is Backend Engineer, Analytics Instrumentation" do
      let(:role) { "Backend Engineer, Analytics Instrumentation" }
      let(:capabilities) { ["reviewer analytics_instrumentation"] }

      it "#reviewer? returns true" do
        expect(subject.reviewer?(project, :analytics_instrumentation, labels)).to be_truthy
      end

      it "#maintainer? returns false" do
        expect(subject.maintainer?(project, :analytics_instrumentation, labels)).to be_falsey
      end
    end
  end

  context "when having single capability" do
    let(:capabilities) { "reviewer backend" }

    it "#reviewer? supports one role per project" do
      expect(subject.reviewer?(project, :backend, labels)).to be_truthy
    end

    it "#traintainer? supports one role per project" do
      expect(subject.traintainer?(project, :database, labels)).to be_falsey
    end

    it "#maintainer? supports one role per project" do
      expect(subject.maintainer?(project, :frontend, labels)).to be_falsey
    end
  end

  context "when having capability in projects without categories" do
    let(:capabilities) { ["reviewer"] }

    it "#reviewer? returns true" do
      expect(subject.reviewer?(project, :none, labels)).to be_truthy
    end

    it "#traintainer? returns false" do
      expect(subject.traintainer?(project, :none, labels)).to be_falsey
    end

    it "#maintainer? returns false" do
      expect(subject.maintainer?(project, :none, labels)).to be_falsey
    end
  end

  describe "#projects" do
    context "when a project is uppercase" do
      let(:project) { "GITLAB" }

      it "is made lowercase" do
        expect(subject.projects).to eq("gitlab" => ["reviewer backend"])
      end
    end

    context "when a capability is uppercase" do
      let(:capabilities) { ["QA"] }

      it "is made lowercase" do
        expect(subject.projects).to eq("gitlab" => ["qa"])
      end
    end
  end

  describe "#local_hour" do
    around do |example|
      Timecop.freeze(Time.utc(2020, 6, 23, 10)) { example.run }
    end

    context "when author is given" do
      where(:tz_offset_hours, :expected_local_hour) do
        -12 | 22
        -10 | 0
        2 | 12
        4 | 14
        12 | 22
      end

      with_them do
        it "returns the correct local_hour" do
          expect(subject.local_hour).to eq(expected_local_hour)
        end
      end
    end
  end

  describe "#markdown_name" do
    context "when timezone info is not given" do
      before do
        options.delete("tz_offset_hours")
      end

      it "returns the markdown name without timezone info" do
        expect(subject.markdown_name).to eq(options["markdown_name"])
      end

      context "when markdown name is not provided in the first place" do
        let(:default_markdown_name) { "`@luigi` [![profile link](https://gitlab.com/gitlab-org/gitlab-svgs/-/raw/main/sprite_icons/user.svg?ref_type=heads)](https://gitlab.com/luigi)" }

        before do
          options.delete("markdown_name")
        end

        it "returns the markdown name built from #initialize" do
          expect(subject.markdown_name).to eq(default_markdown_name)
        end
      end
    end

    it "returns markdown name with timezone info" do
      expect(subject.markdown_name).to eq("#{options["markdown_name"]} (UTC+2)")
    end

    context "when offset is 1.5" do
      let(:tz_offset_hours) { 1.5 }

      it "returns markdown name with timezone info, not truncated" do
        expect(subject.markdown_name).to eq("#{options["markdown_name"]} (UTC+1.5)")
      end
    end

    context "when author is given" do
      where(:tz_offset_hours, :author_offset, :diff_text) do
        -12 | -10 | "2 hours behind author"
        -10 | -12 | "2 hours ahead of author"
        -10 | 2 | "12 hours behind author"
        2 | 4 | "2 hours behind author"
        4 | 2 | "2 hours ahead of author"
        2 | 3 | "1 hour behind author"
        3 | 2 | "1 hour ahead of author"
        2 | 2 | "same timezone as author"
      end

      with_them do
        it "returns markdown name with timezone info" do
          author = described_class.new(options.merge("username" => "mario", "tz_offset_hours" => author_offset))

          floored_offset_hours = subject.__send__(:floored_offset_hours)
          utc_offset = floored_offset_hours >= 0 ? "+#{floored_offset_hours}" : floored_offset_hours

          expect(subject.markdown_name(author: author)).to eq("#{options["markdown_name"]} (UTC#{utc_offset}, #{diff_text})")
        end
      end
    end
  end
end
